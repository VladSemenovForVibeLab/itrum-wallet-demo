<html><img src="docs/bank_one.jpeg" alt="Изображение банка"></html>

# Описание проекта

Данный проект представляет собой RESTful API для управления кошельками пользователей. API позволяет осуществлять операции пополнения и снятия средств с кошелька, а также получать текущий баланс кошелька.

## Стек технологий

- Java 17
- Spring Boot 2.5.5
- PostgreSQL
- Liquibase
- Swagger
- Spring Devtools
- H2
- Spring Data JPA
- Docker

## Установка и запуск

1. Установите Docker и Docker Compose, если они не установлены на вашей системе.

2. Склонируйте репозиторий:

   ```
   git clone https://gitlab.com/VladSemenovForVibeLab/itrum-wallet-demo.git
   ```

3. Перейдите в директорию проекта:

   ```
   cd walletDemo
   ```

4. Запустите проект с помощью Docker Compose:

   ```
   docker-compose up 
   ```

   Это создаст и запустит контейнеры для приложения и базы данных.

5. API будет доступно по адресу `http://localhost:8082`.

## Использование API

### Создание кошелька

**Endpoint:** `POST /api/v1/wallet`

**Заголовки:**
```
Content-Type: application/json
```

**Тело запроса:**
```json
{
  "walletId": "uuid",
  "operationType": "DEPOSIT",
  "amount": 1000
}
```

**Пример успешного ответа:**
```json
{
  "walletId": "uuid",
  "operationType": "DEPOSIT",
  "balance": 1000
}
```

### Получение  кошелька

**Endpoint:** `GET /api/v1/wallets/{WALLET_UUID}`

**Пример успешного ответа:**
```json
{
  "walletId": "uuid",
  "operationType": "DEPOSIT",
  "balance": 1000
}
```

## Конфигурация

Конфигурация приложения и базы данных может быть изменена с помощью файлов `application.properties`.

## Тестирование

API-эндпоинты покрыты тестами, которые можно найти в директории `src/test/java`. Для запуска тестов воспользуйтесь следующей командой:

```
mvn test
```

## Дополнительная информация

- Документация API доступна по адресу `http://localhost:8082/swagger-ui.html`.
- Все зависимости проекта управляются с помощью Maven.
- Логика по изменению счета в базе данных реализована с использованием Spring Data JPA.
- Проблемы при работе в конкурентной среде были учтены и обработаны для предотвращения ошибок 50X.