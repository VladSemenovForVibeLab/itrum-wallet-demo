package ru.semenov.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import ru.semenov.domain.enums.OperationType;
import ru.semenov.domain.model.Wallet;
import ru.semenov.dto.AmountDTO;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.WalletDTO;
import ru.semenov.service.IWalletService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class WalletRestControllerTest {

    private static final String WALLET_UUID = "12345";

    @Mock
    private IWalletService walletService;

    private WalletRestController walletController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        walletController = new WalletRestController(walletService);
    }

    @Test
    void testWithdrawalsShouldReturnOkStatus() {
        AmountDTO amountDTO = new AmountDTO();
        amountDTO.setAmount(BigDecimal.valueOf(100));

        MessageResponse expectedResponse = new MessageResponse(HttpStatus.OK,"Withdrawal successful", LocalDateTime.now());

        when(walletService.withdrawals(any(AmountDTO.class), anyString())).thenReturn(expectedResponse);

        ResponseEntity<MessageResponse> response = walletController.withdrawals(amountDTO, WALLET_UUID);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    void testDepositShouldReturnOkStatus() {
        AmountDTO amountDTO = new AmountDTO();
        amountDTO.setAmount(BigDecimal.valueOf(200));

        MessageResponse expectedResponse = new MessageResponse(HttpStatus.OK,"Deposit successful",LocalDateTime.now());

        when(walletService.deposit(any(AmountDTO.class), anyString())).thenReturn(expectedResponse);

        ResponseEntity<MessageResponse> response = walletController.deposit(amountDTO, WALLET_UUID);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedResponse, response.getBody());
    }

    @Test
    void testBalanceShouldReturnOkStatus() {
        AmountDTO expectedAmountDTO = new AmountDTO();
        expectedAmountDTO.setAmount(BigDecimal.valueOf(500.0));

        when(walletService.balance(anyString())).thenReturn(expectedAmountDTO);

        ResponseEntity<AmountDTO> response = walletController.balance(WALLET_UUID);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedAmountDTO, response.getBody());
    }

    @Test
    void testSortByAmountShouldReturnOkStatus() {
        WalletDTO wallet1 = new WalletDTO();
        wallet1.setAmount(BigDecimal.valueOf(100));

        WalletDTO wallet2 = new WalletDTO();
        wallet2.setAmount(BigDecimal.valueOf(200));

        List<WalletDTO> expectedWallets = Arrays.asList(wallet1, wallet2);

        when(walletService.sortByAmount()).thenReturn(expectedWallets);

        ResponseEntity<List<WalletDTO>> response = walletController.sortByAmount();

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(expectedWallets, response.getBody());
    }


    @Test
    public void testDepositThousandsOfRequests() {
        String walletUuid = "12345";
        AmountDTO amountDTO = AmountDTO.builder()
                .amount(BigDecimal.valueOf(1000))
                .build();

        Wallet wallet = Wallet.builder()
                .valletId(walletUuid)
                .amount(BigDecimal.valueOf(5000))
                .build();
        Optional<Wallet> walletOptional = Optional.of(wallet);

        MessageResponse expectedResponse = new MessageResponse(HttpStatus.OK, "Зачислено 1000000 руб", LocalDateTime.now().toString());

        when(walletService.depositThousandsOfRequests(any(String.class), any(AmountDTO.class))).thenReturn(expectedResponse);

        ResponseEntity<MessageResponse> response = walletController.depositThousandsOfRequests(walletUuid, amountDTO);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void testDepositThousandsOfRequestsFunction() {
        String walletUuid = "12345";
        AmountDTO amountDTO = AmountDTO.builder()
                .amount(BigDecimal.valueOf(1000))
                .build();

        MessageResponse expectedResponse = new MessageResponse(HttpStatus.OK, "Успешно зачислено 1000000", LocalDateTime.now().toString());

        when(walletService.depositThousandsOfRequestsFunction(any(String.class), any(AmountDTO.class))).thenReturn(expectedResponse);

        ResponseEntity<MessageResponse> response = walletController.depositThousandsOfRequestsFunction(walletUuid, amountDTO);
        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(expectedResponse, response.getBody());
    }


}
