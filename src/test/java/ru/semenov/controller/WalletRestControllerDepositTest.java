package ru.semenov.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.semenov.domain.enums.OperationType;
import ru.semenov.domain.model.Wallet;
import ru.semenov.dto.AmountDTO;
import ru.semenov.repository.WalletRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.jayway.jsonpath.internal.path.PathCompiler.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WalletRestControllerDepositTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WalletRepository walletRepository;

    @Test
    public void testDepositEndpointSpam() throws Exception {
        int requestCount = 10000;
        Wallet wallet = new Wallet();
        wallet.setAmount(BigDecimal.ZERO);
        wallet.setOperationType(OperationType.DEPOSIT);
        walletRepository.save(wallet);
        String walletUUID = wallet.getValletId();
        AmountDTO amountRequest = new AmountDTO(BigDecimal.ONE);
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        List<Future<Void>> futures = new ArrayList<>();
        for (int i = 0; i < requestCount; i++) {
            Callable<Void> task = () -> {
                mockMvc.perform(post("/api/v1/wallet/" + walletUUID + "/deposit")
                        .content(asJsonString(amountRequest))
                        .contentType(MediaType.APPLICATION_JSON));
                return null;
            };
            futures.add(executorService.submit(task));
        }
        for (Future<Void> future : futures) {
            future.get();
        }
        executorService.shutdown();
        Optional<Wallet> optionalWallet = walletRepository.findById(walletUUID);
        if (optionalWallet.isPresent()) {
            Wallet updatedWallet = optionalWallet.get();
            BigDecimal expectedAmount = BigDecimal.ONE.multiply(BigDecimal.valueOf(requestCount));
            assertEquals(expectedAmount.toBigInteger(), updatedWallet.getAmount().toBigInteger());
        } else {
            fail("Ошибка с созданием модели кошелька в тесте");
        }
    }
    private static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
