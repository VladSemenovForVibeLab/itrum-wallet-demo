package ru.semenov.util;

import org.springframework.stereotype.Component;
import ru.semenov.domain.model.Wallet;
import ru.semenov.dto.WalletDTO;

import java.util.List;

@Component
public class WalletDTOUtil implements EntityDTOUtil<Wallet, WalletDTO> {

    @Override
    public WalletDTO toDTO(Wallet element) {
        return WalletDTO
                .builder()
                .valletId(element.getValletId())
                .amount(element.getAmount())
                .operationType(element.getOperationType())
                .build();
    }

    @Override
    public Wallet toEntity(WalletDTO dto) {
        return Wallet.builder()
                .valletId(dto.getValletId())
                .amount(dto.getAmount())
                .operationType(dto.getOperationType())
                .build();
    }

    @Override
    public List<WalletDTO> toDTO(List<Wallet> elements) {
        return elements.stream().map(this::toDTO).toList();
    }

    @Override
    public List<Wallet> toEntity(List<WalletDTO> dtos) {
        return dtos.stream().map(this::toEntity).toList();
    }
}
