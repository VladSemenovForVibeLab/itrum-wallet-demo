package ru.semenov.repository;

import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;
import ru.semenov.domain.enums.OperationType;
import ru.semenov.domain.model.Wallet;

import java.util.List;
import java.util.Optional;

@Repository
public interface WalletRepository extends JpaRepository<Wallet, String> {
    List<Wallet> findByOperationType(OperationType operationType);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Wallet> findById(String id);
}