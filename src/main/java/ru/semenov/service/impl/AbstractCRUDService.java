package ru.semenov.service.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.domain.exception.ResourceNotFoundException;
import ru.semenov.dto.MessageResponse;
import ru.semenov.service.CRUDService;
import ru.semenov.util.EntityDTOUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCRUDService<E,K,D> implements CRUDService<D,K> {
    abstract JpaRepository<E,K> getRepository();
    abstract EntityDTOUtil<E,D> getEntityDtoUtil();
    @Override
    @Transactional
    public D createEntity(D objectDto){
        E entity = getEntityDtoUtil().toEntity(objectDto);
        E entitySave = getRepository().save(entity);
        return getEntityDtoUtil().toDTO(entitySave);
    }
    @Override
    @Transactional(readOnly = true)
    public D findById(K id){
        E entityFind = getRepository().findById(id).orElseThrow(()->new ResourceNotFoundException("Данная модель не найдена!"));
        return getEntityDtoUtil().toDTO(entityFind);
    }
    @Override
    @Transactional(readOnly = true)
    public List<D> findAll(){
        List<E> objects = new ArrayList<>();
        getRepository().findAll().forEach(objects::add);
        List<D> entityDtoList = getEntityDtoUtil().toDTO(objects);
        return entityDtoList;
    }
    @Override
    @Transactional
    public D updateEntity(D objectDto){
        E entityUpdate = getEntityDtoUtil().toEntity(objectDto);
        E entitySave = getRepository().save(entityUpdate);
        D dto = getEntityDtoUtil().toDTO(entitySave);
        return dto;

    }
    @Override
    @Transactional
    public MessageResponse deleteEntity(D objectDto){
        E entityForDelete = getEntityDtoUtil().toEntity(objectDto);
        getRepository().delete(entityForDelete);
        return new MessageResponse(HttpStatus.NO_CONTENT,"Модель была удалена!!!", LocalDateTime.now().toString());
    }
}

