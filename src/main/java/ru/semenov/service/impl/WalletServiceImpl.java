package ru.semenov.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.semenov.domain.enums.OperationType;
import ru.semenov.domain.exception.ResourceNotFoundException;
import ru.semenov.domain.model.Wallet;
import ru.semenov.dto.AmountDTO;
import ru.semenov.dto.MessageResponse;
import ru.semenov.repository.WalletRepository;
import ru.semenov.service.IWalletService;
import ru.semenov.util.EntityDTOUtil;
import ru.semenov.util.WalletDTOUtil;
import ru.semenov.dto.WalletDTO;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
public class WalletServiceImpl extends AbstractCRUDService<Wallet, String, WalletDTO> implements IWalletService {
    private final WalletRepository walletRepository;
    private final WalletDTOUtil walletDTOUtil;


    public WalletServiceImpl(WalletRepository walletRepository, WalletDTOUtil walletDTOUtil) {
        this.walletRepository = walletRepository;
        this.walletDTOUtil = walletDTOUtil;
    }

    @Override
    JpaRepository<Wallet, String> getRepository() {
        return walletRepository;
    }

    @Override
    EntityDTOUtil<Wallet, WalletDTO> getEntityDtoUtil() {
        return walletDTOUtil;
    }

    @Override
    @Transactional
    public MessageResponse withdrawals(AmountDTO amountDTO, String id) {
        Wallet wallet = walletRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Данный кошелек не найден!!"));
        BigDecimal newAmount = wallet.getAmount().subtract(amountDTO.getAmount());
        wallet.setAmount(newAmount);
        walletRepository.save(wallet);
        return new MessageResponse(HttpStatus.OK, "Списано " + amountDTO.getAmount() + " руб", LocalDateTime.now().toString());
    }

    @Override
    @Transactional
    public MessageResponse deposit(AmountDTO amount, String walletUuid) {
        Optional<Wallet> walletOptional = walletRepository.findById(walletUuid);
        if (walletOptional.isPresent()) {
            Wallet wallet = walletOptional.get();
            synchronized (wallet) {
                wallet.addToAmount(amount.getAmount());
                walletRepository.save(wallet);
            }
            return new MessageResponse(HttpStatus.OK, "Зачислено " + amount.getAmount() + " руб", LocalDateTime.now().toString());
        }
        return new MessageResponse(HttpStatus.BAD_REQUEST, "Возникли проблемы при зачислении, обратитесь позже, либо проверте данные!");
    }

    @Override
    @Transactional(readOnly = true)
    public AmountDTO balance(String walletUuid) {
        Optional<Wallet> walletOptional = walletRepository.findById(walletUuid);
        if (walletOptional.isPresent()) {
            return new AmountDTO(walletOptional.get().getAmount());
        }
        throw new ResourceNotFoundException(String.format("Данный кошелек не найден!"));
    }

    @Override
    @Transactional(readOnly = true)
    public List<WalletDTO> sortByAmount() {
        return walletDTOUtil.toDTO(walletRepository.findAll(Sort.by("amount")));
    }

    @Override
    @Transactional(readOnly = true)
    public List<WalletDTO> getByOperationType(OperationType operationType) {
        return walletDTOUtil.toDTO(walletRepository.findByOperationType(operationType));
    }

    @Override
    @Transactional
    public MessageResponse depositThousandsOfRequests(String walletUuid, AmountDTO amountDTO) {
        Optional<Wallet> walletOptional = walletRepository.findById(walletUuid);
        if(walletOptional.isPresent()){
            Wallet wallet = walletOptional.get();
            for(int i=0;i<1000;i++){
                BigDecimal newAmount = wallet.getAmount().add(amountDTO.getAmount());
                wallet.setAmount(newAmount);
            }
            walletRepository.save(wallet);
            return new MessageResponse(HttpStatus.OK, "Зачислено " + amountDTO.getAmount().multiply(BigDecimal.valueOf(1000)) + " руб", LocalDateTime.now().toString());
        }
        return new MessageResponse(HttpStatus.BAD_REQUEST, "Возникли проблемы при зачислении, обратитесь позже, либо проверте данные!");
    }

    @Override
    public MessageResponse depositThousandsOfRequestsFunction(String walletUuid, AmountDTO amountDTO) {
        for (int i = 0; i < 1000;i++){
            deposit(amountDTO, walletUuid);
        }
        return new MessageResponse(HttpStatus.OK,"Успешно зачислено "+amountDTO.getAmount().multiply(BigDecimal.valueOf(1000)),LocalDateTime.now().toString());
    }


}
