package ru.semenov.service;

import ru.semenov.domain.enums.OperationType;
import ru.semenov.dto.AmountDTO;
import ru.semenov.dto.MessageResponse;
import ru.semenov.dto.WalletDTO;

import java.util.List;

public interface IWalletService extends CRUDService<WalletDTO,String> {

    MessageResponse withdrawals(AmountDTO amountDTO, String id);

    MessageResponse deposit(AmountDTO amount, String walletUuid);

    AmountDTO balance(String walletUuid);

    List<WalletDTO> sortByAmount();

    List<WalletDTO> getByOperationType(OperationType operationType);

    MessageResponse depositThousandsOfRequests(String walletUuid, AmountDTO amountDTO);

    MessageResponse depositThousandsOfRequestsFunction(String walletUuid, AmountDTO amountDTO);
}
