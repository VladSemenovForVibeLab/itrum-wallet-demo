package ru.semenov.service;

import ru.semenov.dto.MessageResponse;

import java.util.List;

public interface CRUDService<D,K> {
    D createEntity(D entityDto);
    D findById(K id);

    List<D> findAll();
    D updateEntity(D entityDto);

    MessageResponse deleteEntity(D entityDto);
}

