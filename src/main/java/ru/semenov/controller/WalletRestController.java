package ru.semenov.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.semenov.domain.enums.OperationType;
import ru.semenov.dto.AmountDTO;
import ru.semenov.dto.MessageResponse;
import ru.semenov.service.CRUDService;
import ru.semenov.service.IWalletService;
import ru.semenov.dto.WalletDTO;

import java.util.List;

@RestController
@RequestMapping(WalletRestController.WALLET_URL)

public class WalletRestController extends CRUDRestController<WalletDTO,String>{
    public static final String WALLET_URL = "/api/v1/wallet";
    private final IWalletService walletService;

    public WalletRestController(IWalletService walletService) {
        this.walletService = walletService;
    }

    @Override
    CRUDService<WalletDTO, String> getService() {
        return walletService;
    }

    @PostMapping("/{WALLET_UUID}/withdrawals")
    public ResponseEntity<MessageResponse> withdrawals(@RequestBody AmountDTO amountDTO,
                                                       @PathVariable String WALLET_UUID){
        return new ResponseEntity<>(walletService.withdrawals(amountDTO,WALLET_UUID),HttpStatus.OK);
    }

    @PostMapping("/{WALLET_UUID}/deposit")
    public ResponseEntity<MessageResponse> deposit(@RequestBody AmountDTO amount,
                                                   @PathVariable String WALLET_UUID){
        synchronized (this) {
            return new ResponseEntity<>(walletService.deposit(amount, WALLET_UUID), HttpStatus.OK);
        }
    }

    @GetMapping("/{WALLET_UUID}/balance")
    public ResponseEntity<AmountDTO> balance(@PathVariable String WALLET_UUID){
        return new ResponseEntity<>(walletService.balance(WALLET_UUID),HttpStatus.OK);
    }

    @GetMapping("/all/sortedByBalance")
    public ResponseEntity<List<WalletDTO>> sortByAmount(){
        return new ResponseEntity<>(walletService.sortByAmount(),HttpStatus.OK);
    }

    @GetMapping("/byOperationType")
    public ResponseEntity<List<WalletDTO>> getByOperationType(@RequestParam OperationType operationType){
        return new ResponseEntity<>(walletService.getByOperationType(operationType),HttpStatus.OK);
    }

    @PostMapping("/{WALLET_UUID}/depositThousandsOfRequests")
    public ResponseEntity<MessageResponse> depositThousandsOfRequests(@PathVariable String WALLET_UUID,
                                                                     @RequestBody AmountDTO amountDTO){
        return new ResponseEntity<>(walletService.depositThousandsOfRequests(WALLET_UUID, amountDTO),HttpStatus.OK);
    }

    @PostMapping("/{WALLET_UUID}/depositThousandsOfRequestsFunction")
    public ResponseEntity<MessageResponse> depositThousandsOfRequestsFunction(@PathVariable String WALLET_UUID,
                                                                                @RequestBody AmountDTO amountDTO){
        return new ResponseEntity<>(walletService.depositThousandsOfRequestsFunction(WALLET_UUID, amountDTO),HttpStatus.OK);
    }
}
