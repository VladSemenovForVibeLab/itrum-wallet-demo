package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.domain.enums.OperationType;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Schema(description = "WalletDTO")
public class WalletDTO {
    @Schema(description = "Wallet id", example = "e58ed763-928c-4155-bee9-fdbaaadc15f3")
    private String valletId;
    @Schema(description = "Wallet ampunt", example = "70000")
    private BigDecimal amount;
    @Schema(description = "Wallet - >  operation type", example = "DEPOSIT")
    private OperationType operationType;
}
