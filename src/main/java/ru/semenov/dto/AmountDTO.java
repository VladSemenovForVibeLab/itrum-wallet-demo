package ru.semenov.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.semenov.domain.model.Wallet;

import java.math.BigDecimal;
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Schema(description = "AmountDTO")
public class AmountDTO {
    @Schema(description = "Amount",example = "70000")
    private BigDecimal amount;
    public AmountDTO(Wallet wallet){
        this.amount = wallet.getAmount();
    }
}
