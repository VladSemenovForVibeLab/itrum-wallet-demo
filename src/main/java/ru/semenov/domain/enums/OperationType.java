package ru.semenov.domain.enums;

public enum OperationType {
    DEPOSIT, WITHDRAW
}
