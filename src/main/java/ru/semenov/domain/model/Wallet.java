package ru.semenov.domain.model;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.UuidGenerator;
import ru.semenov.domain.enums.OperationType;

import java.math.BigDecimal;

/**
 * Класс представляет 'КОШЕЛЕК'. Аннотирован как сущность JPA .
 * '@Entity' - показывает, что данный класс является JPA-сущностью и будет отображаться в таблице базы данных.
 * '@Data' - аннотация Lombok, которая генерирует методы getter, setter, equals, hashCode и toString.
 * '@AllArgsConstructor' - аннотация Lombok, которая генерирует конструктор с аргументами.
 * '@NoArgsConstructor' - аннотация Lombok, которая генерирует конструктор без аргументов.
 * '@Table' - указывает название таблицы, к которой будет отображаться данная сущность.
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = Wallet.TABLE_NAME)
public class Wallet {
    public static final String TABLE_NAME = "wallet_table";
    private static final String WALLET_ID_COLUMN_NAME = "vallet_id";
    private static final String WALLET_AMOUNT_COLUMN_NAME = "wamount";
    @Id
    @org.springframework.data.annotation.Id
    @UuidGenerator
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    @Setter(AccessLevel.NONE)
    @Column(name = WALLET_ID_COLUMN_NAME, unique = true, nullable = false)
    private String valletId;
    @Column(name = WALLET_AMOUNT_COLUMN_NAME,nullable = false)
    private BigDecimal amount;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private OperationType operationType;
    @Transient
    private final Object lock = new Object();
    public void addToAmount(BigDecimal value) {
        synchronized (lock) {
            this.amount = this.amount.add(value);
        }
    }
}
