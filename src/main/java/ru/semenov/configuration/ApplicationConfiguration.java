package ru.semenov.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Класс используется для настройки Swagger Open API.
 */

@Configuration
public class ApplicationConfiguration {
    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("WALLET APPLICATION")
                        .description("Spring Boot Application")
                        .version("0.0.1")
                        .contact(new Contact()
                                .name("Владислав")
                                .email("ooovladislavchik@gmail.com")
                                .url("https://t.me/ProstoVladTut")));
    }

}
